# Testing Postgresql Replication Setup

Steps to run

- start virtual machine via vagrant `vagrant up`
- export ssh config `vagrant ssh-config > ssh-config`
- run setup playbook `ansible-playbook setup.yml`
