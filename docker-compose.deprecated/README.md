# Create Postgresql replication

Sample .envrc

```sh
export COMPOSE_PROJECT_NAME=psqlreplica
export POSTGRES_PASSWORD=example
export POSTGRES_USER=example
export POSTGRES_DB=example
export MASTER_PORT=5432
export SLAVE1_PORT=5433
export PGDATA=/var/lib/postgresql/data
```

## Prepare folders

```sh
mkdir -p ./data/{master,shared}
```

## Start master node first

```sh
docker-compose up -d master
```
## Login to master node to create replicator user

Run these SQLs on master node

```sql
CREATE USER replicator WITH REPLICATION PASSWORD 'my_replicator_password';
SELECT pg_create_physical_replication_slot('slave1');
```

## Create slave data folder via pg_basebackup

Run these commands in master node shell

```sh
pg_basebackup -D /shared/slave1 -S slave1 -X stream -P -U replicator -Fp -R --user example
```

## Update master connection in slave data folder

Update file `data/shared/slave1/postgresql.auto.conf`

```
primary_conninfo = 'host=master port=5432 user=replicator password=my_replicator_password'
restore_command = 'cp /var/lib/postgresql/data/pg_wal/%f "%p"'
```
## Start slave1

```sh
docker-compose up -d slave1
```
